const chalk = require("chalk");
const chokidar = require("chokidar");

// this is just in case somebody doesn't read the docs...
const inject = () =>
  console.log(
    chalk.red(
      "You must pass an `inject` param in the options object! Please see README.`"
    )
  );

const defaultOptions = {
  watch: null, // files to watch -- passed as first arg to chokidar.watch()
  DEBUG: false, // debug flag
  inject // function
};

function override(obj, fxn) {
  // check for __original first in case somebody else is
  // monkeypatching using the same paradigm...
  const original_fxn =
    obj.prototype[fxn.name].__original || obj.prototype[fxn.name];

  // bind the new function to the current this, passing it the
  // original function as the first argument, and then call it, again
  // with the current this, passing in any additional arguments.
  function wrapper() {
    return fxn.bind(this, original_fxn).apply(this, arguments);
  }

  // stash the original function so if we need to stack up additional
  // overrides, we can
  wrapper.__original = original_fxn;

  // finally, overwrite the original function with the wrapped version!
  obj.prototype[fxn.name] = wrapper;
}

// set up a watcher that will call the injected function whenever a
// watched file is added or changed
function initializeWatcher(eleventyInstance, options) {
  options.debug && console.log(chalk.yellow("Initializing watcher"));

  const watcher = chokidar.watch(options.watch, { persistent: true });

  watcher
    .on("add", file => options.inject(eleventyInstance, options, file))
    .on("change", file => options.inject(eleventyInstance, options, file));
}

module.exports = {
  configFunction: (eleventyConfig, userOptions) => {
    // we want to run this function ASAMFP, so...
    setImmediate(() => {
      const Eleventy = require("@11ty/eleventy/src/Eleventy.js");

      // ...if for whatever reason we didn't get Eleventy loaded,
      // we're gonna no-op on out...
      if (Eleventy.prototype) {
        const options = { ...defaultOptions, ...userOptions };

        if (options.DEBUG) {
          console.log(chalk.red("ACTIVE PLUGIN-INJECT OPTIONS:"));
          console.dir(options);
        }

        let initialized = false;

        // this function is called when running `eleventy --serve`, so
        // we again need to set a watcher that will call the injected
        // function when watched files are added or changed. if we're
        // not watching any files, we don't do squat. the only reason
        // this function is any different than watch(), immediately
        // above, is because we need to pass along the `port` argument
        // to the original function.
        function serve(original, port) {
          options.DEBUG && console.log(chalk.yellow("IN SERVE"));

          if (!initialized && options.watch) {
            initializeWatcher(this, options);
            initialized = true; // only initialize once
          }
          return original.apply(this, [port]);
        }

        override(Eleventy, serve);

        // this function is called when running `eleventy --watch`, so
        // we need to set a watcher that will call the injected
        // function whenever one of our watched files is added or
        // changed. if we weren't told to watch any files, don't
        // bother doing anything.
        function watch(original_fxn) {
          options.DEBUG && console.log(chalk.yellow("IN WATCH"));

          if (!initialized && options.watch) {
            initializeWatcher(this, options);
            initialized = true; // only initialize once
          }

          return original_fxn.apply(this);
        }

        if (options.watch) {
          override(Eleventy, watch);
        }

        // this function is called when writing out files in a normal
        // one-off build, so we need to call the injected function
        // here directly for each file in the watchlist, and then call
        // the original function so it can do its thing
        function write(original_fxn) {
          options.DEBUG && console.log(chalk.yellow("IN WRITE"));

          if (!initialized && !this.isDryRun) {
            if (Array.isArray(options.watch)) {
              for (const file of options.watch) {
                options.inject(this, options, file);
              }
            } else {
              options.inject(this, options, options.watch);
            }
          }

          return original_fxn.apply(this);
        }

        override(Eleventy, write);
      }
    });
  }
};
