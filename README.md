# NOTE: This package is deprecated and no longer being maintained. As of v1.\*, Eleventy contains out-of-the-box functionality that is functionally identical to what this plugin provided

# eleventy-plugin-injector

An [Eleventy](https://github.com/11ty/eleventy) plugin to inject
arbitrary code into the site build process.

## Installation

Available on [npm](https://www.npmjs.com/package/@infinity-interactive/eleventy-plugin-injector).

```
npm install @infinity-interactive/eleventy-plugin-injector --save
```

Open up your Eleventy config file, `require` the plugin, and then use
`addPlugin` to activate and configure it:

```
const pluginInjector = require("@infinity-interactive/eleventy-plugin-injector");
module.exports = function(eleventyConfig) {
  eleventyConfig.addPlugin(pluginInjector, {
    watch: "that-one-file.scss",
    inject: function(instance, options, file) {
      // do stuff here to write new file(s) from your watched files
      //
      // file will contain the path to the file that caused the
      // function to fire
    }
  });
};
```

Read more about [Eleventy plugins](https://www.11ty.io/docs/plugins/).

## Options

Options should be set in an object that is passed as the second
parameter in the `addPlugin` call that loads the plugin. There are
three keys that can be passed in the `options` object:

- `inject`: This should be a function that accepts three parameters. It
  will be called whenever the build process is triggered (whether by
  running `eleventy` with no arguments, running `eleventy --watch`, or
  running `eleventy --serve`). The function will be passed a copy of
  the current Eleventy instance, the `options` object itself, and the
  path to which watched file triggered the event.
  **NOTE:** you must pass in a meaningful value here or this plugin
  will not do anything.
- `watch`: Optional parameter detailing which file(s) to watch. This
  is directly passed as the first parameter to the
  [chokidar](https://www.npmjs.com/package/chokidar) `watch` method,
  so anything that's legal there is okay here ("file, dir, glob, or
  array"). If this parameter is not provided, the `--serve` and
  `--watch` methods of running Eleventy will NOT have the `inject`
  function injected into them.
- `DEBUG`: Defaults to `false`. Set it to `true` for a bit more
  verbosity about what's happening under the hood.

## Example

Here's how you might use this plugin in your `.eleventy.js` file to
invoke [sass](https://www.npmjs.com/package/sass) and
[cleanCss](https://www.npmjs.com/package/clean-css) during the build:

```
const cleanCss = require("clean-css");
const fs = require("fs-extra");
const path = require("path");
const pluginInjector = require("@infinity-interactive/eleventy-plugin-injector");
const sass = require("sass");

const sassFile = "site.scss";
const cssFile = "./_site/css/site.css";

module.exports = function(config) {
  eleventyConfig.addPlugin(pluginInjector, {
    watch: sassFile,
    inject: (eleventyInstance, options, file) => {
      if (!fs.existsSync(path.dirname(cssFile))) {
        // Create cssPath directory recursively
        try {
          fs.mkdirSync(path.dirname(cssFile), { recursive: true });
        } catch (error) {
          console.error(`Error making directory for CSS output: ${error}`);
        }
      }
      const renderedSass = sass.renderSync({ file: sassFile });
      const renderedCss = renderedSass.css.toString();
      const minCss = new cleanCss({}).minify(renderedCss).styles;
      fs.writeFile(cssFile, minCss).catch(error =>
        console.error(`Error writing generated CSS: ${error}`)
      );
    }
  });

  // rest of config here
}
```

## Disclaimer

This plugin depends on names of methods internal to Eleventy, so if
those are changed upstream, it will stop working as expected. (I think
it's pretty unlikely they'll change, however.)

## Contributing

Patches and pull requests welcome.

## Acknowledgments

Thanks to Maarten Schroeven for
[eleventy-plugin-sass](https://www.npmjs.com/package/eleventy-plugin-sass)
which both inspired this library and provided valuable hints about how
to implement it.

## License

MIT © [Infinity Interactive](https://iinteractive.com) / John SJ Anderson
